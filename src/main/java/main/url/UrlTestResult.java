package main.url;

import java.util.Date;

public class UrlTestResult {
	
	private Date date;
	
	private String url;
	
	private String result;


	public UrlTestResult(Date date, String url, String result) {
		this.date = date;
		this.url = url;
		this.result = result;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getResult() {
		return result;
	}


	public void setResult(String result) {
		this.result = result;
	}

	

}
