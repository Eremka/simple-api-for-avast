package main.url;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestBodyUriSpec;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;

@RestController
public class UrlController {

	@RequestMapping(method = RequestMethod.POST, value = "/url")
	public List<UrlTestResult> isUrlValid(@RequestBody List<String> url)
			throws InterruptedException, ExecutionException, TimeoutException {

		List<UrlTestResult> resList = new LinkedList<UrlTestResult>();

		ExecutorService executor = Executors.newFixedThreadPool(3);

		for (String u : url) {

			Future<String> futureTask1 = executor.submit(() -> {
				// Below I check whether the url starts with https://, else the app would crash.
				RequestBodyUriSpec client = WebClient.create(u.startsWith("https://") ? u : "https://" + u).method(HttpMethod.GET);
				ResponseSpec response = client.retrieve();
				return response.bodyToFlux(String.class).blockFirst() != null ? "Ok" : "Broken Link";
			});

			String res = futureTask1.get(5, TimeUnit.SECONDS);

			resList.add(new UrlTestResult(new Date(), u, res));
		}

		executor.shutdown();
		return resList;
	}
}
