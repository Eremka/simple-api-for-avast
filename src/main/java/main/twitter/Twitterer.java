package main.twitter;

import java.util.ArrayList;
import java.util.List;

import twitter4j.Query;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class Twitterer {
	
	private Twitter twitter;
	private Query query;
	
	public Twitterer() {
		twitter = TwitterFactory.getSingleton();
		query = new Query();
	}
	
	public List<TwitterResponse> getNrReturnFromQuery(String queryString) throws TwitterException {		
		query.setQuery(queryString);
		
		List<Status> statusRes = twitter.search(query).getTweets();
		List<TwitterResponse> res = new ArrayList<TwitterResponse>();
		
		for (Status s: statusRes) {
			res.add(new TwitterResponse(s.getText(), s.getUser().getName(), s.getCreatedAt()));
		}
				
		return res;
	}

}
