package main.twitter;

import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import twitter4j.TwitterException;

@RestController
public class TwitterController {
	
	Twitterer twitterer = new Twitterer();
	
	@RequestMapping("/twitter/{query}")
	public List<TwitterResponse> getTweetsWithQuery(@PathVariable String query) throws TwitterException {		
		return twitterer.getNrReturnFromQuery(query);
	}
	
	@RequestMapping("twitter/count/{query}")
	public TwitterCountResponse getNrOfTweetsWithQuery(@PathVariable String query) throws TwitterException {
		List<TwitterResponse> responseList = twitterer.getNrReturnFromQuery(query);
		
		return new TwitterCountResponse(responseList.size(), new Date(), responseList);
	}

}
