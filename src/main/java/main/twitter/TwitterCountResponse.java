package main.twitter;

import java.util.Date;
import java.util.List;

public class TwitterCountResponse {
	
	private int count;
	private Date fromDate;
	private Date toDate;
	private List<TwitterResponse> responseList;
	
	public TwitterCountResponse(int size, Date fromDate, List<TwitterResponse> responseList) {
		this.count = size;
		this.fromDate = fromDate;
		this.toDate = getCalculatedTodate(fromDate);
		this.responseList = responseList;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	
	
	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<TwitterResponse> getResponseList() {
		return responseList;
	}

	public void setResponseList(List<TwitterResponse> responseList) {
		this.responseList = responseList;
	}

	private Date getCalculatedTodate(Date fromDate) {
		// Calculating current time minus 15 minutes as the api only displays the results for the last 15 min.
		return new Date(fromDate.getTime() - 60000 * 15);
	}

}
