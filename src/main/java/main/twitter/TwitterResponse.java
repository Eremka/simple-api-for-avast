package main.twitter;

import java.util.Date;

public class TwitterResponse {
	
	private String message;
	private String user;
	private Date date;
	
	
	public TwitterResponse(String text, String name, Date createdAt) {
		this.message = text;
		this.user = name;
		this.date = createdAt;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
