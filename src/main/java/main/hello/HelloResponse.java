package main.hello;

public class HelloResponse {
	
	private String greeting;
	private String message;
	
	public HelloResponse(String greeting, String message) {
		this.greeting = greeting;
		this.message = message;
	}
	
	public String getGreeting() {
		return greeting;
	}
	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
