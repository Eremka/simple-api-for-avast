package main.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	// You have to start every app with a hello. It's polite...
	@RequestMapping("/hello")
	public String sayHello() {
		StringBuilder sb = new StringBuilder();
		sb.append("Hi there!");
		sb.append(System.getProperty("line.separator"));
		sb.append("This is an example of my test api.");
		sb.append(System.getProperty("line.separator"));
		sb.append("Please use the following end points: ");
		sb.append(System.getProperty("line.separator"));
		sb.append("'http://localhost:8080/url' for the post request testing the urls");
		sb.append(System.getProperty("line.separator"));
		sb.append("As input, the system expects a string or an array of strings");
		sb.append(System.getProperty("line.separator"));
		sb.append(System.getProperty("line.separator"));
		sb.append("http://localhost:8080/twitter/{queryString} to get all tweets");
		sb.append(System.getProperty("line.separator"));
		sb.append("with that word in the message from the past 15 min");
		sb.append(System.getProperty("line.separator"));
		sb.append(System.getProperty("line.separator"));
		sb.append("http://localhost:8080/twitter/count/{queryString} to get the number");
		sb.append("of tweets returned, and the time period queried");
		sb.append(System.getProperty("line.separator"));
		sb.append(System.getProperty("line.separator"));
		sb.append("Enjoy your day :)");
		
		return sb.toString();
	}
}
